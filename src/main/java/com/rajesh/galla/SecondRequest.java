package com.rajesh.galla;

import com.rajesh.galla.utils.Utilities;
import com.rajesh.galla.utils.ResourceHandler;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;
import org.apache.http.*;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;

/**
 * Created by rajeshgalla on 8/10/15.
 */
public class SecondRequest implements Runnable{

    private static List<Long> responseTime = new ArrayList<Long>();

    public static void main(String args[]) throws InterruptedException {

        Thread  thread[] = new Thread[100];
        SecondRequest secondRequest = new SecondRequest();
        for (int i =0; i < 100 ; i++) {
            thread[i] = new Thread(secondRequest,String.valueOf(i));
            thread[i].start();
        }

        for (int i = 0; i < 99; i++) { //wait till all threads die
            while (thread[i].isAlive()) {
                Thread.sleep(100);
            }
        }

        Long[] responseTimeArray = secondRequest.responseTime.toArray(new Long[0]);
        Arrays.sort(responseTimeArray);

        DescriptiveStatistics stats = new DescriptiveStatistics();
        for( int i = 0; i < responseTimeArray.length; i++) {
            stats.addValue(responseTimeArray[i]);
        }

        double mean = stats.getMean();
        double standardDeviation = stats.getStandardDeviation();

        System.out.println("10th percentile " + stats.getPercentile(10));
        System.out.println("50th percentile " + stats.getPercentile(50));
        System.out.println("90th percentile " + stats.getPercentile(90));
        System.out.println("95th percentile " + stats.getPercentile(95));
        System.out.println("99th percentile " + stats.getPercentile(99));

        System.out.println("mean " + mean);
        System.out.println("standardDeviation " + standardDeviation);
    }

    @Override
    public void run() {

        try {
            this.request();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void request() throws IOException {

        final String emailID = Utilities.getEmailID();

        FirstRequest firstRequest = new FirstRequest();
        HttpResponse response = firstRequest.getResponse();

        JSONObject finalResult = Utilities.getJSONFromResponse(response);

        CloseableHttpClient httpclient = HttpClients.custom()
                .addInterceptorLast(new HttpRequestInterceptor() {

                    public void process(
                            final HttpRequest request,
                            final HttpContext context) throws HttpException, IOException {
                        request.addHeader("X-Surya-Email-Id", emailID);
                    }
                })
                .build();

        String uuid = finalResult.getString("uuid");

        JSONObject postJSON = new JSONObject();
        postJSON.put("emailId", emailID);
        postJSON.put("uuid", uuid);

        StringEntity jsonEntity = new StringEntity(postJSON.toString());
        HttpPost httpPost = new HttpPost(ResourceHandler.getResource("url"));
        httpPost.setHeader("Content-type", "application/json");
        httpPost.setEntity(jsonEntity);

        long startTime = System.currentTimeMillis();
        response = httpclient.execute(httpPost);
        long elapsedTime = System.currentTimeMillis() - startTime;

        this.responseTime.add(elapsedTime);

        if ( !(response.getStatusLine().getStatusCode() == 200) || !EntityUtils.toString(response.getEntity()).equals("Success"))
            System.out.println("Not success");
    }
}
