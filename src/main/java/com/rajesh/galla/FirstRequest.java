package com.rajesh.galla;

import com.rajesh.galla.utils.Utilities;
import com.rajesh.galla.utils.ResourceHandler;
import org.apache.http.*;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;

/**
 * Created by rajeshgalla on 8/10/15.
 */
public class FirstRequest {

    public HttpResponse getResponse() throws IOException {

        final String emailID = Utilities.getEmailID();

        CloseableHttpClient httpclient = HttpClients.custom()
                .addInterceptorLast(new HttpRequestInterceptor() {

                    public void process(
                            final HttpRequest request,
                            final HttpContext context) throws HttpException, IOException {
                        request.addHeader("X-Surya-Email-Id", emailID);
                    }

                })
                .build();

        HttpGet httpget = new HttpGet(ResourceHandler.getResource("url"));
        return httpclient.execute(httpget);
    }
}
