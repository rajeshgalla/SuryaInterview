package com.rajesh.galla.utils;

import org.apache.http.HttpResponse;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;
import java.util.Scanner;

/**
 * Created by rajeshgalla on 8/10/15.
 */
public class Utilities {
    public static String getEmailID() throws IOException {

        return ResourceHandler.getResource("emailID");
    }

    public static JSONObject getJSONFromResponse(HttpResponse response) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
        String json = reader.readLine();
        JSONTokener tokener = new JSONTokener(json);
        JSONObject finalResult = new JSONObject(tokener);
        return finalResult;
    }
}
